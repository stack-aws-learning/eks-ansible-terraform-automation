## Configuration

This configuration section will install and configure the necessary tools for our Amazon EKS (Elastic Kubernetes Service) cluster on AWS using Ansible

### Prerequisites

Before getting started, ensure you have the following prerequisites:

-  **Ansible**: Ansible is an open-source automation tool used for configuration management, provisioning, and application deployment. You can install Ansible by following the instructions on the [official Ansible website](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).
