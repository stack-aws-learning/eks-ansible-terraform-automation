from __future__ import (absolute_import, division, print_function)
__metaclass__ = type


import os
from ansible.errors import AnsibleUndefinedVariable, AnsibleParserError, AnsibleConnectionFailure
from ansible.plugins.vars import BaseVarsPlugin

try:
    import boto3
    HAS_BOTO3 = True
except ImportError:
    HAS_BOTO3 = False

DOCUMENTATION = '''
    name: read_terraform_state
    version_added: "2.10"  # for collections, use the collection version, not the Ansible version
    short_description: Read terraform state
    description: Loads ansible vars from remote terraform state 
    notes: 
        - requires boto3
'''

DIR = os.path.dirname(os.path.realpath(__file__))

CACHE = {}

class VarsModule(BaseVarsPlugin):

    """
    Loads variables for groups and/or hosts
    """

    def __init__(self, *args):
        super(VarsModule, self).__init__(*args)
        # Initialize the AWS SSM client
        ssm_client = boto3.client('ssm')

        # Retrieve the bucket name from AWS Parameter Store
        response = ssm_client.get_parameter(Name='/terraform/s3-bucket-name')

        # Extract the bucket name from the response
        self.bucket_name = response['Parameter']['Value']
        err = []
        ansible_env = os.getenv('ANSIBLE_ENV')
        if ansible_env is None:
            raise ValueError("Environment variable 'ANSIBLE_ENV' is not set")

        # Construct the target string
        target = "terraform/infrastructure-state-" + ansible_env + ".tfstate"

        # Set the target in the CACHE dictionary
        CACHE['target'] = target
        if (CACHE['target'] is None):
            err.append('ANSIBLE_ENV')
        self.aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID')
        self.aws_region = os.getenv('AWS_DEFAULT_REGION')
        if (self.aws_region is None):
            err.append('AWS_DEFAULT_REGION')
        if (len(err) > 0):
            raise AnsibleUndefinedVariable('Env variables missing: ' + (','.join(err)))
    def get_vars(self, loader, path, entities):
        if not HAS_BOTO3:
            raise AnsibleParserError('Vars plugin requires boto3')
        # Using cached value if necessary
        if ('result' in CACHE):
            return CACHE['result']
        else:
            print('Reading ansible_vars from remote Terraform state')
        try:
            # Call s3 and retrieve resource
            s3 = boto3.client('s3')
            content_object = s3.get_object(Bucket=self.bucket_name, Key=CACHE['target'])
            tfstate_content = content_object['Body'].read().decode('utf-8')
            tfstate = loader.load(tfstate_content)
            ansible_vars = tfstate['outputs']['ansible_vars']['value']
            vars = loader.load(ansible_vars)
            CACHE['result'] = vars
            return CACHE['result']
        except Exception as err:
            raise AnsibleConnectionFailure(err)
