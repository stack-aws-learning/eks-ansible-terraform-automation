- name: Install traefik chart
  kubernetes.core.helm:
    state: present
    release_name: traefik
    chart_ref: traefik
    chart_version: "{{ traefik.helm_version }}"
    chart_repo_url: https://traefik.github.io/charts
    release_namespace: traefik
    create_namespace: true
    values:
      globalArguments:
      ingressClass:
        enabled: true
        isDefaultClass: true
      deployment:
        kind: DaemonSet # Déploiement en mode DaemonSet
      providers:
        kubernetesCRD:
          enabled: true # Active la création des CRD pour les objets personnalisés
      service:
        type: LoadBalancer # Type de service à utiliser pour Traefik
        annotations:
          service.beta.kubernetes.io/aws-load-balancer-type: nlb # By default traefik uses classic LoadBalancer
      ingressRoute:
        dashboard:
          enabled: false # Désactive la création d'une IngressRoute pour accéder au dashboard
  become: false

- name: Create Traefik secret for dashboard authentication
  kubernetes.core.k8s:
    state: present
    api_version: v1
    kind: Secret
    namespace: traefik
    definition:
      metadata:
        name: traefik-auth-creds
      type: kubernetes.io/basic-auth
      stringData:
        username: "{{ dashboard_user_name }}"
        password: "{{ dashboard_password }}"

- name: Create traefik middelware
  kubernetes.core.k8s:
    state: present
    api_version: traefik.containo.us/v1alpha1
    kind: Middleware
    namespace: traefik
    definition:
      metadata:
        name: dashboard-basic-auth
      spec:
        basicAuth:
          secret: traefik-auth-creds

- name: Expose Traefik dashboard
  kubernetes.core.k8s:
    state: present
    api_version: traefik.containo.us/v1alpha1
    kind: IngressRoute
    namespace: traefik
    definition:
      metadata:
        name: dashboard
      spec:
        entryPoints:
          - web
        routes:
          - match: PathPrefix(`/dashboard`, `/dashboard/`) || PathPrefix(`/api`, `/api/`)
            kind: Rule
            services:
              - name: api@internal
                kind: TraefikService
            middlewares:
              - name: dashboard-basic-auth
                namespace: traefik

- name: Get traefik service resources
  kubernetes.core.k8s_info:
    api_version: v1
    kind: Service
    name: traefik
    namespace: traefik
  register: traefik_service
  until: traefik_service is defined and ((traefik_service.resources | length) > 0)
  retries: 60
  delay: 5

- name: Register the load balancer host name
  ansible.builtin.set_fact:
    traefik_hostname: "{{ traefik_service.resources[0].status.loadBalancer.ingress[0].hostname }}"
