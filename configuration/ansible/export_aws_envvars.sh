#!/bin/bash

# Run AWS CLI commands to retrieve AWS credentials and region
aws_configure_list=$(aws configure list)
aws_sts_output=$(aws sts get-session-token)

# Parse the temporary credentials from the output
aws_access_key=$(echo "$aws_sts_output" | jq -r '.Credentials.AccessKeyId')
aws_secret_key=$(echo "$aws_sts_output" | jq -r '.Credentials.SecretAccessKey')
aws_session_token=$(echo "$aws_sts_output" | jq -r '.Credentials.SessionToken')

# Parse AWS region from the output
aws_region=$(echo "$aws_configure_list" | awk '/region/{print $2}')

# Export AWS environment variables
export AWS_ACCESS_KEY_ID="$aws_access_key"
export AWS_SECRET_ACCESS_KEY="$aws_secret_key"
export AWS_SESSION_TOKEN="$aws_session_token"
export AWS_DEFAULT_REGION="$aws_region"
export ANSIBLE_ENV="dev"

echo "AWS environment variables exported:"
echo "AWS_ACCESS_KEY_ID"="$AWS_ACCESS_KEY_ID"
echo "AWS_SECRET_ACCESS_KEY"="$AWS_SECRET_ACCESS_KEY"
echo "AWS_SESSION_TOKEN"="$AWS_SESSION_TOKEN"
echo "AWS_DEFAULT_REGION"="$AWS_DEFAULT_REGION"
echo "ANSIBLE_ENV"="$ANSIBLE_ENV"
