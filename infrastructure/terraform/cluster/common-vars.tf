variable "aws_region" {
  description = "The name of the AWS Region"
  type        = string
  default     = "eu-west-3"
}

variable "profile" {
  description = "The name of the AWS profile in the credentials file"
  type        = string
  default     = "default"
}

variable "cluster_name" {
  description = "The name of the EKS Cluster"
  type        = string
  default     = "stack-cluster"
}

variable "environment" {
  description = "Environment to deploy to"
  type        = string
  default     = "dev"
}


variable "eks_version" {
  type    = string
  default = "1.29"
}

variable "cidr" {
  type    = string
  default = "10.50.0.0/16"
}

variable "private_subnets" {
  type    = list(any)
  default = ["10.50.2.0/23", "10.50.4.0/23", "10.50.6.0/23"]
}

variable "public_subnets" {
  type    = list(any)
  default = ["10.50.11.0/24", "10.50.12.0/24", "10.50.13.0/24"]
}