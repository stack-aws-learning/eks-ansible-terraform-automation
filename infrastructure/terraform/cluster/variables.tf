variable "vpc_id" {
  type = string
}

variable "instance_types" {
  default = ["t2.medium"]
}

variable "cluster_node_group_max_size" {
  description = "Maximum number of nodes in our node group"
  type        = number
}

variable "cluster_node_group_desired_size" {
  description = "Desired number of nodes in our node group"
  type        = number
}