data "aws_eks_cluster" "cluster" {
  name = var.cluster_name
}

data "aws_arn" "cluster" {
  arn = data.aws_eks_cluster.cluster.arn
}
