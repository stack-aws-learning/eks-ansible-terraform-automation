data "aws_ssm_parameter" "tf-backend-bucket-name" {
  name = "/terraform/s3-bucket-name"
}

data "aws_ssm_parameter" "tf-backend-dynamodb-table-name" {
  name = "/terraform/dynamodb-table-name"
}