terraform {
  # specify minimum version of Terraform 
  required_version = ">= 1.8.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      #  Lock version to prevent unexpected problems
      version = "5.45.0"
    }

    local = {
      source  = "hashicorp/local"
      version = "~> 2.5.1"
    }

  }
}

provider "aws" {
  region = var.aws_region
}