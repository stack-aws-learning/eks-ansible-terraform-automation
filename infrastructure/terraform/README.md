# Terraform

This section of the project will create the EKS cluster and all its necessary resources

## Getting Started

### Configuration

1. **Generate the backend file for dev environment**:

   Generate the backend file for the dev environment.
     ```
     ./gen_backend.sh
     ```

2. **Terraform init**:

   Init the terraform code with the following command line
     ```
     terraform init -backend-config=backend -reconfigure -input=false
     ```

### Making Changes

Make the changes you want ot the infrastructure.

**Preview Changes**:

Before pushing you code, preview your changes using the following command
     ```
     terraform plan -input=false -var-file=vars/dev.tfvars
     ```

### Contributing

**Version Control**:

   Push your changes to a GitLab repository using SSH. Ensure that your changes are reviewed through a merge request (MR) before being applied.
