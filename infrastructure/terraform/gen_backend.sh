#!/bin/bash

# Define function to convert report using jq
convert_report() {
    jq -r '([.resource_changes[]?.change.actions?]|flatten)|{"create":(map(select(.=="create"))|length),"update":(map(select(.=="update"))|length),"delete":(map(select(.=="delete"))|length)}'
}

# List S3 buckets
aws s3 ls

# Get S3 bucket name from AWS Parameter Store
TF_STATE_BUCKET=$(aws ssm get-parameter --name /terraform/s3-bucket-name --query "Parameter.Value" --output text)

# Get DynamoDB table name from AWS Parameter Store
TF_STATE_DYNAMODB_TABLE=$(aws ssm get-parameter --name /terraform/dynamodb-table-name --query "Parameter.Value" --output text)

# Get default AWS region
AWS_DEFAULT_REGION=$(aws configure get region)


# Define backend configuration
cat <<EOF > backend
bucket = "$TF_STATE_BUCKET"
key    = "terraform/infrastructure-state-dev.tfstate"
region = "$AWS_DEFAULT_REGION"
dynamodb_table = "$TF_STATE_DYNAMODB_TABLE"
encrypt = true
EOF

# Display backend configuration
cat backend
