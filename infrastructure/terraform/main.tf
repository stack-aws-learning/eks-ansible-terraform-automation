locals {
  cluster_name = var.cluster_name
}

// Will contruct the VPC that will host our EKS cluster
module "network" {
  source          = "./network"
  cluster_name    = local.cluster_name
  cidr            = var.cidr
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets
}


// Will contruct the EKS cluster
module "cluster" {
  source                          = "./cluster"
  cluster_name                    = local.cluster_name
  environment                     = var.environment
  private_subnets                 = module.network.private_subnets
  vpc_id                          = module.network.vpc_id
  cluster_node_group_desired_size = var.cluster_node_group_desired_size
  cluster_node_group_max_size     = var.cluster_node_group_max_size
  instance_types                  = var.instance_types
}

module "managed_resources" {
  count        = length(var.managed_cluster_names)
  source       = "./cluster/cluster_details"
  cluster_name = var.managed_cluster_names[count.index]
}