## Infrastructure

This infrastructure section provisions an Amazon EKS (Elastic Kubernetes Service) cluster on AWS using Terraform and Pulumi. It aims to simplify the setup process by providing infrastructure as code (IaC) for deploying EKS clusters with ease.

### Prerequisites

Before getting started, ensure you have the following prerequisites:

- AWS account with appropriate permissions to create EKS clusters and associated resources.
- Pulumi installed on your local machine. You can install Pulumi from [here](https://www.pulumi.com/docs/install/) and follow the installation instructions. [v3.113.0]
-  You need either a personal Pulumi account or an account within an Organization. If you don't have one, you can sign up for a free account on the [Pulumi website](https://app.pulumi.com/signup).
- Terraform installed on your local machine. You can install Terraform from [here](https://developer.hashicorp.com/terraform/install) and follow the installation instructions. [v1.8.0]
- AWS CLI installed and configured with appropriate IAM user credentials. You can install AWS CLI and configure it by following the instructions [here](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html).

- Node.js installed on your local machine. You can install Node from [here](https://nodejs.org/en/download) and follow the installation instructions. [v1.8.0]