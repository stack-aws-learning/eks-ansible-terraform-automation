import * as aws from "@pulumi/aws";

const varPrefix = "infra-terraform-state-";

// Create a KMS key that will be used for encryption of our resources
const kmsKey = new aws.kms.Key(varPrefix + "key", {
  description:
    "KMS key to encrypt S3 bucket and DynamuDB table dedicated to terraform state",
});

const kmsAlias = new aws.kms.Alias(varPrefix + "alias", {
  targetKeyId: kmsKey.id,
  name: "alias/" + varPrefix + "key",
});

// Create an S3 bucket for the Terraform state files
const bucket = new aws.s3.BucketV2(varPrefix + "bucket", {
  serverSideEncryptionConfigurations: [
    {
      rules: [
        {
          bucketKeyEnabled: false,
          applyServerSideEncryptionByDefaults: [
            {
              kmsMasterKeyId: kmsKey.id,
              sseAlgorithm: "aws:kms",
            },
          ],
        },
      ],
    },
  ],
});

const serverSideEncryptionConfigurations =
  new aws.s3.BucketServerSideEncryptionConfigurationV2(
    varPrefix + "sse-configuration",
    {
      bucket: bucket.id,
      rules: [
        {
          bucketKeyEnabled: false,
          applyServerSideEncryptionByDefault: {
            kmsMasterKeyId: kmsKey.id,
            sseAlgorithm: "aws:kms",
          },
        },
      ],
    }
  );

const exampleBucketPublicAccessBlock = new aws.s3.BucketPublicAccessBlock(
  varPrefix + "bucket-public-access",
  {
    bucket: bucket.id,
    blockPublicAcls: true,
    blockPublicPolicy: true,
    ignorePublicAcls: true,
    restrictPublicBuckets: true,
  }
);

const bucketVersioningV2 = new aws.s3.BucketVersioningV2(
  varPrefix + "bucket-versioning",
  {
    bucket: bucket.id,
    versioningConfiguration: { status: "Enabled" },
  }
);

// Create a DynamoDB table for state locking
const table = new aws.dynamodb.Table(varPrefix + "table", {
  name: varPrefix + "lock",
  pointInTimeRecovery: { enabled: true },
  hashKey: "LockID",
  serverSideEncryption: {
    enabled: true,
    kmsKeyArn: kmsKey.arn,
  },
  attributes: [{ name: "LockID", type: "S" }],
  billingMode: "PAY_PER_REQUEST",
});

// Export the bucket name, DynamoDB table name and the kms key id
export const bucketName = bucket.id;
export const tableName = table.name;
export const keyId = kmsKey.id;

// Store bucket name and DynamoDB table name in Parameter Store
const bucketNameParameter = new aws.ssm.Parameter("terraform-s3-bucket-name", {
  name: "/terraform/s3-bucket-name",
  type: "String",
  value: bucketName,
});

const tableNameParameter = new aws.ssm.Parameter(
  "terraform-dynamo-table-name",
  {
    name: "/terraform/dynamodb-table-name",
    type: "String",
    value: tableName,
  }
);
